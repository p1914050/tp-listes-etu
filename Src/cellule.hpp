#ifndef LIFAP6_LISTE_CELLULE_HPP
#define LIFAP6_LISTE_CELLULE_HPP

class Cellule {

  /* votre code ici */
  public :
    
    /* construction d'une cellule vide */
    Cellule() ;

    /* construction d'une cellule vide */
    Cellule(int val) ;

    /* construction par copie */
    Cellule(const Cellule& autre) ;

    /* affectation */
    Cellule& operator=(const Cellule& autre) ;

    /* destruction */
    ~Cellule() ;

    int valeur ;
    Cellule* next ;

} ;

#endif
