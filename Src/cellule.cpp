#include "cellule.hpp"

Cellule::Cellule() {
    valeur = 0 ;
    next = nullptr ;
}

Cellule::Cellule(int val) {
    valeur = val ;
    next = nullptr ;
}

Cellule::Cellule(const Cellule& autre) {
 valeur = autre.valeur ;
 next = autre.next ;
}

Cellule& Cellule::operator=(const Cellule& autre) {
  /* votre code ici */
  return *this ;
}

Cellule::~Cellule() {
    delete next ;
}



