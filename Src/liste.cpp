#include "liste.hpp"

#include <iostream>
#include <cassert>

Liste::Liste() {
  head = nullptr ;
}

Liste::Liste(const Liste& autre) {
  head = autre.head ;
}

Liste& Liste::operator=(const Liste& autre) {
  if (this != &autre){
    delete head ;
    Cellule * cell = autre.head ;
      while (cell != nullptr){
      ajouter_en_queue(cell->valeur) ;
      cell = cell->next ;
    }
  }
  return *this ;
}

Liste::~Liste() {
  delete head ;
}

void Liste::ajouter_en_tete(int valeur) {
  Cellule * new_cell = new Cellule(valeur) ;
  new_cell->next = head ;
  head = new_cell ;
}

void Liste::ajouter_en_queue(int valeur) {
  if (head == nullptr){
    head = new Cellule(valeur) ;
  }
  else{
    Cellule * new_cell = new Cellule(valeur) ;
    Cellule * cell = head ;
    while (cell->next != nullptr){
      cell = cell->next ;
    }
  cell->next = new_cell ;
  }
  
}

void Liste::supprimer_en_tete() {
  head = head->next ;
}

Cellule* Liste::tete() {
  return head ;
}

const Cellule* Liste::tete() const {
  return head ;
}

Cellule* Liste::queue() {
  Cellule* cell = head ;
  while (cell->next != nullptr){
    cell = cell->next ;
  }
  return cell ;
}

const Cellule* Liste::queue() const {
  Cellule* cell = head ;
  while (cell->next != nullptr){
    cell = cell->next ;
  }
  return cell ;
}

int Liste::taille() const {
  Cellule* cell = head ;
  int compteur = 0 ;
  while (cell != nullptr){
    cell = cell->next ;
    compteur++;
  }
  return compteur ;
}

Cellule* Liste::recherche(int valeur) {
  Cellule* cell = head ;
  while (cell != nullptr && cell->valeur != valeur){
    cell = cell->next ;
  }
  return cell ;
}

const Cellule* Liste::recherche(int valeur) const {
  Cellule* cell = head ;
  while (cell != nullptr && cell->valeur != valeur){
    cell = cell->next ;
  }
  return cell ;
}

void Liste::afficher() const {
  Cellule* cell = head ;
  while (cell != nullptr){
    std::cout << cell->valeur << std::endl ;
    cell = cell->next ;
  }
}
